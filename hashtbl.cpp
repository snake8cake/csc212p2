/****************************************************
implementation file for the hash table class.
*****************************************************/

#include "hashtbl.h"
#include <iostream>
using std::endl;
using std::ostream;
#include <string.h> /* for strcpy */
#include <algorithm> /* for find */
using std::find;
#include <iomanip>
using std::setfill;
using std::setw;
#include <cassert>

#define R32 (rand()*100003 + rand())
#define R64 (((uint64_t)R32 << 32) + R32)
#define TLEN ((size_t)(1 << this->nBits))

namespace csc212
{
	/* First we implement our hash family */
	hashTbl::hash::hash(unsigned rangebits, const uint32_t* a,
					const uint64_t* alpha, const uint64_t* beta) {
		this->rangebits = rangebits;
		if (a) {
			for (size_t i = 0; i < aLen; i++) {
				this->a[i] = a[i] | 1; /* make sure it is odd. */
			}
		} else {
			for (size_t i = 0; i < aLen; i++) {
				this->a[i] = R32 | 1;
			}
		}
		this->alpha = ((alpha) ? *alpha : R64) | 1;
		this->beta = ((beta) ? *beta : R64);
		/* only keep the low order bits of beta: */
		this->beta &= ((uint64_t)-1)>>(64 - rangebits);
	}
	uint64_t hashTbl::hash::operator()(const string& x) const {
		assert(x.length() <= 4*aLen);
		/* TODO: write the hash function. */

		size_t k = x.length();
		size_t kx;

		if(k%4 != 0)
			kx = k + (4 - k%4);		//rounds up kx to a multiple of 4
		else
			kx = k;

		char c[k];
		char cx[kx];						//gauranteed to hold full 4-byte words
		strcpy(c, x.c_str());		//copy input string into char array
		for(size_t i = 0; i < k; i++)
			cx[i] = c[i];					//fill beginning of cx

		if(kx!=k)
			for(size_t i = k; i < kx; i++)
				cx[i] = 0;					//fills end of cx with 0's as needed

		uint32_t* s = new uint32_t[kx/4];			//4 bytes per int so kx/4
		s = reinterpret_cast<uint32_t*>(cx);	//cast cx as int array into s

		k = kx/4;		//now k matches with the summation from the readme

		uint64_t accumulator, left, right;	//used bc operations in mod 64
		accumulator = left = right = 0;

		//SUMMATION
		for(size_t i = 0; i < k/2; i++)
		{
			left = s[2*i] + a[2*i];
			right = s[2*i + 1] + a[2*i + 1];
			accumulator += (left*right);
		}

		//SUMMATION - ADD THIS EXTRA TERM FOR ODD# OF WORDS
		if((k%2) !=0)
		{
			left = s[k-1] + a[k-1];
			right = a[k];
			accumulator += (left*right);
		}

		uint64_t div = -1;		//2^64 - 1
		for(size_t i = 0; i < rangebits; i++)
			div /= 2;						//divides by 2 m times, 2^64 --> 2^(64 -m)

		div++;								//adds back the 1 we subtracted before

		return (alpha*accumulator + beta)/div;	//g(x)
		return 0;
	}

	//constructors:
	hashTbl::hashTbl(unsigned nBits, const uint32_t* ha,
					const uint64_t* halpha, const uint64_t* hbeta) :
		nBits(nBits),h(nBits,ha,halpha,hbeta)
	{
		this->table = new list<val_type>[TLEN];
	}
	hashTbl::hashTbl(const hashTbl& H)
	{
		/* TODO: write this */
		this->nBits = H.nBits;
		this->table = new list<val_type>[TLEN];
		for(size_t i=0; i<TLEN; i++)
			this->table[i] = H.table[i];
		/* NOTE: the underlying linked list class has a working
		 * assignment operator! */
	}

	//destructor:
	hashTbl::~hashTbl()
	{
		delete[] this->table;
		//NOTE: this will call the destructor of each of the linked lists,
		//so there isn't anything else that we need to worry about.
	}

	//operators:
	hashTbl& hashTbl::operator=(hashTbl H)
	{
		/* TODO: write this */
		this->table=H.table;
		this->nBits=H.nBits;
		H.table = NULL;
		return *this;
	}

	ostream& operator<<(ostream& o, const hashTbl& H)
	{
		for (size_t i = 0; i < H.tableLength(); i++) {
			o << "[" << setfill('0') << setw(2) << i << "] |";
			for (list<val_type>::iterator j = H.table[i].begin();
					j != H.table[i].end(); j++) {
				o << *j << "|";
			}
			o << endl;
		}
		return o;
	}

	void hashTbl::insert(val_type x)
	{
		/* TODO: write this */
		int i = h(x);
		if(!search(x))
			table[i].push_front(x);
	}

	void hashTbl::remove(val_type x)
	{
		/* TODO: write this */
		for(size_t i=0; i<TLEN; i++)
			this->table[i].remove(x);
	}

	void hashTbl::clear()
	{
		/* TODO: write this */
		for(size_t i=0; i<TLEN; i++)
			this->table[i].clear();
	}

	bool hashTbl::isEmpty() const
	{
		/* TODO: write this */
		for(size_t i=0; i < TLEN; i++)
			if (!(this->table[i].empty()))
				return false;
		return true;
	}

	bool hashTbl::search(val_type x) const
	{
		/* TODO: write this */
		size_t i = h(x);
		list<val_type>::iterator it;
		for(it = table[i].begin(); it != table[i].end(); it++)
			if(*it == x)
					return true;

		return false;
	}

	size_t hashTbl::countElements() const
	{
		/* TODO: write this */
		size_t i, numElements = 0;
		for(i=0; i<TLEN; i++)
			numElements = numElements	+ table[i].size();

		return numElements;
	}


	size_t hashTbl::tableLength() const
	{
		return TLEN;
	}

	size_t hashTbl::countCollisions() const
	{
		/* TODO: write this */
		size_t i,nCollisions=0;
		for(i=0; i<TLEN; i++)
			if(table[i].size() > 1)
				++nCollisions;

		return nCollisions;
	}

	size_t hashTbl::longestListLength() const
	{
		/* TODO: write this */
		size_t longest = table[0].size();
		for(size_t i=1; i<TLEN; i++)
			if (longest<table[i].size())
				longest = table[i].size();

		return longest;
	}
}
